#!/bin/bash
DOMAIN=k3d.local.with-humans.org
while true
do
  echo -n "Live:   "
  curl "https://welcome.${DOMAIN}" ; echo

  echo -n "Canary: "
  curl "https://welcome-canary.${DOMAIN}/" ; echo

  echo -n "Stable: "
  curl "https://welcome-stable.${DOMAIN}/" ; echo
  echo "--------------------------------------------------------"
  sleep 1
done
